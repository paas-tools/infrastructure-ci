# Infrastructure CI

This repository will hold all the templates that different projects will then `include` in their CI's. [CIPAAS-581](https://its.cern.ch/jira/projects/CIPAAS/issues/CIPAAS-581), [WFP-19](https://its.cern.ch/jira/projects/WFP/issues/WFP-19s).

The purpose of this project is to reduce the amount of duplicated code inside the different CI files between all the projects which inherently will help to standardise how code is organized in the different project.

The templates present in this repository are supposed to be used through the flag [include](https://docs.gitlab.com/ee/ci/yaml/#include) in the `.gitlab-ci.yml`. Instructions on how to use each template are provided below.

## Templates available

Currently, the templates provided are the following:

- Template that builds a custom docker image, using Kaniko (recommended);
- Template that builds a custom docker image; using privileged runners;
- Template that allows a helm chart to be deployed manually in the different environments;
- Template that sets up an OKD4 cluster for automatic operator testing;

Below you can find instructions on how to use each template.

### Custom Docker image

To use the CI template that builds a Docker image and publishes it in the  GitLab registry you must include the following in your CI file:

```yml
include:
  - project: 'paas-tools/infrastructure-ci'
    file: 'docker-images-ci-templates/DockerImages.gitlab-ci.yml'
```

Example of a project using this template can be found in [here](https://gitlab.cern.ch/paas-tools/networking/oracle-table-synchronizer/blob/master/.gitlab-ci.yml)

This will build a Docker image with the tag `RELEASE-TIMEz` when code is pushed to `master` (i.e `RELEASE.2019.10.04T13-42-56Z`) and it will generate a tag with the name of the feature branch when code is pushed to a feature branch (i.e `feature-x`). As per [WFP-19](https://its.cern.ch/jira/projects/WFP/issues/WFP-19s).

#### Docker build with privileged runners

We also have a CI template that builds Docker images using GitLab privileged runners, although this is not the recommended way of building Docker images we have to support it as according to [this issue](https://github.com/GoogleContainerTools/kaniko/issues/856) we are not able to build images where we have to pull from quay.io. So in these cases we have to use this template, e.g:

```yml
include:
  - project: 'paas-tools/infrastructure-ci'
    file: 'docker-images-ci-templates/DockerImages-privileged-runners.gitlab-ci.yml'
```

NOTE: This was true as of Dec 2020, if in the meatime the [issue](https://github.com/GoogleContainerTools/kaniko/issues/856) was fixed we can simply only use Kaniko.

#### Supporting Git submodules

In case the project uses git submodules you will also have to set the following variables:
- `CONTEXT`: Folder that contains the project;
- `DOCKERFILE_PATH`: Path to the Docker file from the `CONTEXT`.

As an example consider the following snippet from a project that uses a git submodule:

```yml
include:
  - project: 'paas-tools/infrastructure-ci'
    file: 'docker-images-ci-templates/DockerImages.gitlab-ci.yml'

variables:
  CONTEXT: $CI_PROJECT_DIR/openshift-acme
  DOCKERFILE_PATH: images/openshift-acme-controller/Dockerfile
```

Example of a project using this template can be found in [here](https://gitlab.cern.ch/paas-tools/networking/openshift-acme-build/blob/master/.gitlab-ci.yml)

### Helm deployment

To use the helm deployment template there some prerequisites which have to be fulfilled before using the template.

#### Prerequisites
In the project where you wish to use this template, you need to create two GitLab CI variables/files 
for each Openshift environment (Playground/Staging/Prod). To do this go over to `Settings` then `CI/CD` tab, expand the `Variables` tab and then you will need to specify the following variables/files scoped to each environment using the `Scope` [feature](https://docs.gitlab.com/ee/ci/environments.html#scoping-environments-with-specs) of GitLab:

1. a variable `TILLER_TOKEN` with the token for the `tiller` service account that is going to be used to deploy the project;
2. a file `HELM_SECRETS`  with the secret values that you want to be used to customize the deployment of the helm chart. Taking the example of the project [DNS manager](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager) this file should look as follows:

```yaml
external-dns:
  views:
    external:
      key: xxxxxxxxxxx
    internal:
      key: yyyyyyyyyyy
oracle-table-synchronizer:
  oracle:
    username: bob_account
    password: zzzzzzzzzzzz
```

#### Template
To use the CI template allows a helm chart to be deployed manually in the different environments you must include the following in your CI file:

```yml
include:
  - project: 'paas-tools/infrastructure-ci'
    file: 'helm-ci-templates/Base.gitlab-ci.yml'
```

Furthermore, you will also have to set the following variables:
- `NAMESPACE`: Namespace where the helm chart will be deployed to;
- `HELM_RELEASE_NAME`: Release name of the helm chart.

As an example consider the following snippet from the project [DNS manager](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager):

```yml
include:
  - project: 'paas-tools/infrastructure-ci'
    file: 'helm-ci-templates/Base.gitlab-ci.yml'

variables:
  NAMESPACE: paas-infra-dnsmanager
  # name of the helm release
  HELM_RELEASE_NAME: dnsmanager
```

#### Supporting environments folder

Given the complexity of some projects (e.g [DNS manager](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager)) sometimes we need to have a folder which holds the different values for the different environments. If we want these values to be considered in the helm deployment you have to create a folder named `environments` in the root of the project. Then, you will have to create three folders each with the exact name of each environment (Playground/Staging/Prod). Finally, you can put in each folder the `yaml` files that you want to be considered in each environment. You can check the [DNS manager](https://gitlab.cern.ch/paas-tools/infrastructure/dns-manager) as an example.
